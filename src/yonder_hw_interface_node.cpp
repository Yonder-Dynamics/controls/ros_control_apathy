#include <yonder_hw_interface/yonder_hw_interface.h>
#include <ros/callback_queue.h>



int main(int argc, char** argv)
{
  ros::init(argc, argv, "yonder_hw_interface");
  ros::NodeHandle nh;
  ros::CallbackQueue queue;
  nh.setCallbackQueue(&queue);


  // NOTE: We run the ROS loop in a separate thread as external calls such
  // as service callbacks to load controllers can block the (main) control loop
  ros::AsyncSpinner spinner(4, &queue);
  spinner.start();


  yonder_hw_interface::YonderHWInterface rover(nh);

  ros::Time ts = ros::Time::now();
  ros::Rate rate(50);
  while (ros::ok())
  {
    ros::Duration d = ros::Time::now() - ts;
    ts = ros::Time::now();
    rover.update(ts, d);
    rate.sleep();
  }


  ros::waitForShutdown();
  
  return 0;
}

#include <sstream>
#include <yonder_hw_interface/yonder_hw_interface.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include <iostream>


using namespace hardware_interface;
using joint_limits_interface::JointLimits;
using joint_limits_interface::SoftJointLimits;
using joint_limits_interface::PositionJointSoftLimitsHandle;
using joint_limits_interface::PositionJointSoftLimitsInterface;

namespace yonder_hw_interface
{
	YonderHWInterface::YonderHWInterface(ros::NodeHandle& nh): nh_(nh)
	{
		init();
		// drive_arduino(DRIVE_ARDUINO_PATH, BAUD, serial::Timeout::simpleTimeout(1000));
		// arm_arduino(ARM_ARDUINO_PATH, BAUD, serial::Timeout::simpleTimeout(1000));
		controller_manager_.reset(new controller_manager::ControllerManager(this, nh_));

		nh_.param("/yonder/hardware_interface/loop_hz", loop_hz_, 10.0);
		ros::Duration update_freq = ros::Duration(1.0/loop_hz_);

		drive_effort.resize(6);

		nh_.subscribe<std_msgs::Float64MultiArray>("manual_drive_train", 1, boost::bind(&YonderHWInterface::manual_callback, this, _1));
		nh_.subscribe<std_msgs::Float64MultiArray>("autonomous_drive_train", 1, boost::bind(&YonderHWInterface::autonomous_callback, this, _1));
		nh_.subscribe<std_msgs::Int16>("drive_train_multiplexer", 1, boost::bind(&YonderHWInterface::switch_systems_callback, this, _1));
	}

	YonderHWInterface::~YonderHWInterface()
	{
		// drive_arduino.close();
		// arm_arduino.close();
	}

	void YonderHWInterface::init()
	{
		//joint_mode_ = 3; // ONLY EFFORT FOR NOW
		// Get joint names
		nh_.getParam("/yonder/hardware_interface/joints", joint_names_);
		if (joint_names_.size() == 0)
		{
		  	ROS_FATAL_STREAM_NAMED("init","No joints found on parameter server for controller. Did you load the proper yaml file?");
		}
		num_joints_ = joint_names_.size();

		// Resize vectors
		joint_position_.resize(num_joints_);
		joint_velocity_.resize(num_joints_);
		joint_effort_.resize(num_joints_);
		joint_position_command_.resize(num_joints_);
		joint_effort_command_.resize(num_joints_);


		// Initialize controller
		for (int i = 0; i < num_joints_; ++i)
		{
			std::string joint_name = joint_names_[i];

		  	ROS_DEBUG_STREAM_NAMED("constructor","Loading joint name: " << joint_name);

			nh_.getParam("/yonder/joint_offsets/" + joint_name, joint_offset_[i]);
			nh_.getParam("/yonder/joint_read_ratio/" + joint_name, joint_read_ratio_[i]);

		  	// Create joint state interface
			JointStateHandle jointStateHandle(joint_name, &joint_position_[i], &joint_velocity_[i], &joint_effort_[i]);
		  	joint_state_interface_.registerHandle(jointStateHandle);

		  	// Create position joint interface
			JointHandle jointPositionHandle(jointStateHandle, &joint_position_command_[i]);
			JointLimits limits;
 	   		SoftJointLimits softLimits;
			if (getJointLimits(joint_name, nh_, limits) == false) {
				ROS_ERROR_STREAM("Cannot set joint limits for " << joint_name);
			} else {
				PositionJointSoftLimitsHandle jointLimitsHandle(jointPositionHandle, limits, softLimits);
				positionJointSoftLimitsInterface.registerHandle(jointLimitsHandle);
			}
		  	position_joint_interface_.registerHandle(jointPositionHandle);

		  	// Create effort joint interface
			JointHandle jointEffortHandle(jointStateHandle, &joint_effort_command_[i]);
		  	effort_joint_interface_.registerHandle(jointEffortHandle);

		}

		registerInterface(&joint_state_interface_);
		registerInterface(&position_joint_interface_);
		registerInterface(&effort_joint_interface_);
		registerInterface(&positionJointSoftLimitsInterface);
		std::cout <<"fucko mode" << std::endl;
	}

	void YonderHWInterface::update(const ros::Time& e, const ros::Duration& d)
	{
		read();
		controller_manager_->update(e, d);
		write(elapsed_time_);
	}


	void YonderHWInterface::read()
	{
		// TODO FIGURE OUT HOW YOU DO THIS SHIT
		// for (int i = 0; i < num_joints_; i++)
		// {
		// 	joint_position_[i] = 0;
		// }
	}

	void YonderHWInterface::write(ros::Duration elapsed_time)
	{
		// positionJointSoftLimitsInterface.enforceLimits(elapsed_time);
		// write all the joint commands here after it gets hashed
		// arm_arduino.send(joint_effort_command_);

		

		auto curr_time = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()
		);

		if((curr_time - timeout).count() > 1000){
			for(int i = 0; i < 6; i ++){
				double effort = joint_effort_command_[i];
				joint_position_[i] += effort * .01;
				if(i == 0){
					std::cout << effort << std::endl;
				}
				drive_effort[i] = 0;
			}
		}
		// drive_arduino.send(drive_effort);
	}

	void YonderHWInterface::set_speeds(const std_msgs::Float64MultiArray::ConstPtr & msg){
		std::cout << "bitcharoo" << std::endl;
		float left = msg->data[0];
		float right = msg->data[1];
		drive_effort[0] = -right;
		drive_effort[1] = right;
		drive_effort[2] = right;
		drive_effort[3] = -left;
		drive_effort[4] = -left;
		drive_effort[5] = -left;
		timeout = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()
		);
	}

	void YonderHWInterface::manual_callback(const std_msgs::Float64MultiArray::ConstPtr & msg){
		if(driveState == 0){
			set_speeds(msg);
		}
	}

	void YonderHWInterface::autonomous_callback(const std_msgs::Float64MultiArray::ConstPtr & msg){
		if(driveState == 1){
			set_speeds(msg);
		}
	}

	void YonderHWInterface::switch_systems_callback(const std_msgs::Int16::ConstPtr & msg){
		driveState = msg->data;
	}
}
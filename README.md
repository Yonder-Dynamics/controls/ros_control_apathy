# rover control interface

Install instructions

1. Clone the project into ~/catkin_ws/src/
```
git clone https://gitlab.com/Yonder-Dynamics/controls/ros_control_apathy.git ~/catkin_ws/src
```

2. Go to ~/catkin_ws and build
```
cd ~/catkin_ws && catkin_make
```

3. Source
```
source ~/catkin_ws/devel/setup.zsh
```

4. Install to ROS
```
rosdep install yonder_hw_interface
```

currently configured to work with [moveit](https://gitlab.com/Yonder-Dynamics/controls/moveit)
But is just implementing roscontrol so should be usable with anything that publishes a jointTrajectory

To launch `roslaunch yonder_hw_interface robot.launch`

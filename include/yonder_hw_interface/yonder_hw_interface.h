
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include <controller_manager/controller_manager.h>
#include <boost/scoped_ptr.hpp>
#include <ros/ros.h>
#include <ros/init.h>
#include <yonder_hw_interface/yonder_hw.h>
#include <chrono>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Int16.h>
#include <ros/subscribe_options.h>


using namespace hardware_interface;
using joint_limits_interface::JointLimits;
using joint_limits_interface::SoftJointLimits;
using joint_limits_interface::PositionJointSoftLimitsHandle;
using joint_limits_interface::PositionJointSoftLimitsInterface;


namespace yonder_hw_interface
{
	static const double POSITION_STEP_FACTOR = 10;
	static const double VELOCITY_STEP_FACTOR = 10;
	static const std::string DRIVE_ARDUINO_PATH = "/dev/serial/by-path/platform-3f980000.usb-usb-0:1.2:1.0-port0";
	static const std::string ARM_ARDUINO_PATH = "/dev/serial/by-path/platform-3f980000.usb-usb-0:1.4:1.0-port0";
	static const int BAUD = 115200;

	class YonderHWInterface: public yonder_hw_interface::YonderHW
	{
		public:
			YonderHWInterface(ros::NodeHandle& nh);
			~YonderHWInterface();
			void init();
			void update(const ros::Time& e, const ros::Duration& d);
			void read();
			void write(ros::Duration elapsed_time);
			void set_mode(int mode);
			void set_speeds(const std_msgs::Float64MultiArray::ConstPtr & msg);
			void manual_callback(const std_msgs::Float64MultiArray::ConstPtr & msg);
			void autonomous_callback(const std_msgs::Float64MultiArray::ConstPtr & msg);
			void switch_systems_callback(const std_msgs::Int16::ConstPtr & msg);

		protected:
			ros::NodeHandle nh_;
			ros::Timer non_realtime_loop_;
			ros::Duration control_period_;
			ros::Duration elapsed_time_;

			std::chrono::milliseconds timeout; 

			int driveState; // 0:manual, 1:autonomous

			std::vector<double> drive_effort;

			// serial::Serial drive_arduino;
			// serial::Serial arm_arduino;

			PositionJointInterface positionJointInterface;
			PositionJointSoftLimitsInterface positionJointSoftLimitsInterface;
			double loop_hz_;
			boost::shared_ptr<controller_manager::ControllerManager> controller_manager_;
			double p_error_, v_error_, e_error_;
			std::string _logInfo;
	};
}